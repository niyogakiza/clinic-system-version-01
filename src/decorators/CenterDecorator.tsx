import * as React from 'react'

export const CenterDecorator = (story: any) => (
  <div style={{ textAlign: 'center' }}>{story()}</div>
)
