/**
 * Mostly just Prettier's default rules with a few minor
 * changes, explicitly defined just so we can see what they
 * actually are.
 */
module.exports = {
    printWidth: 80,
    tabWidth: 2,
    useTabs: false,
    semi: false,
    singleQuote: true,
    quoteProps: 'as-needed',
    jsxSingleQuote: true,
    trailingComma: 'all',
    bracketSpacing: true,
    jsxBracketSameLine: false,
    arrowParens: 'avoid',
}
